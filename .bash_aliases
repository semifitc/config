
# vim: set filetype=sh:
#
# This file, residing in a local copy of 
# 'https://gitlab.com/semitfitc/config',
# should be pointed to by a symlink, '~/.bash_aliases'.
#
# This file contains not just aliases but also other
# bashrc-type code appropriate only for my configuration,
# which is common to all of my shells, regardless of the
# particularities of the machine.
#
# The overall idea is that '~/.bashrc' should be a
# symlink to '/etc/skel/.bashrc', so that it is up to 
# date with the operating system's idea of how the shell
# should be initialized. At least on Debian-like systems,
# that idea presently involves sourcing '~/.bash_aliases'
# near the bottom of '/etc/skel/.bashrc'. I use that as
# my hook to customize the configuration of the shell.
#
# Each of my specific configurations for a particular 
# machine should be pointed to by a symlink matching 
# '~/.config/bash_aliases*', so that it will be sourced
# by '~/.bash_aliases'
#

echo "$(readlink -f ${BASH_SOURCE[0]}): Vaughan's generic config"

# Make sure that '/usr/local/bin' is in PATH
if ! echo $PATH | grep /usr/local/bin > /dev/null
then
	export PATH="/usr/local/bin:${PATH}"
fi

# See 'htpps://gitlab.com/tevaughan/config' for use of 
# go configuration setup

alias ls='/bin/ls --color'
alias la='/bin/ls --color -Ahs'
alias ll='/bin/ls --color -Aho'


# Change terminal's title, and a print a status-line
# before PS1 is printed
__prompt_command() {
	local e="$?"

	# Maximum number of characters to print on line
	# before prompt.

	# Allow five characters for printing error-code.
	t=$(($(tput cols) - 5)) 

	# If discernible, put name of OS into term's title
	RELFILE=/etc/os-release
	if [ -r ${RELFILE} ]; then
		os=$(. $RELFILE; echo $NAME $VERSION)
		echo -ne "\033]0;${os}\007"
	fi
	
	# Store in 'cwd' up to n characters at end of PWD.
	n=$((t-1))
	if ((${#PWD} < n)); then n=${#PWD}; fi
	cwd="${PWD: -$n}"

	# Print line before prompt.
	if (($(tput colors) > 7)); then
		tput setaf 2; tput setab 0
	fi
	tput rev; echo -n "$cwd"; tput sgr0
	# Possibly print error-code, and terminate
	# line before prompt.
	if [ $e != 0 ]; then
		# Print return-code in red.
		tput setaf 1; echo " $e"; tput sgr0
	else
		echo ""
	fi
}

export PROMPT_COMMAND=__prompt_command
export PS1="\u@\h \$ "

for a in ~/.config/bash_aliases*; do
	if [ -r "$a" ]; then . "$a"; fi
done

vim() {
  nv=$(which nvim.appimage 2> /dev/null)
  ov=$(which  vim 2> /dev/null)
  if [ "x$nv" != "x" ]; then
    "$nv" $*
  elif [ "x$ov" != "x" ]; then
    "$ov" $*
  else
    echo "ERROR: neither nvim nor vim found in path"
  fi
}


# export EDITOR=$(if ! which nvim 2> /dev/null; then which vi; fi)

# Make the sort-command use the traditional ordering.
# Ubuntu-18.04 supports C.UTF-8 but not C.utf8.
if `locale -a | grep C.utf8 > /dev/null`; then
  export LC_ALL=C.utf8
elif `locale -a | grep C.UTF-8 > /dev/null`; then
  export LC_ALL=C.UTF-8
else
  export LC_ALL=C
fi

# Handle both ordinary configuration and Zettelkasten-configuration.
config() {
  for d in ~/Src/config-* ~/Src/norg/bitbucket ~/Src/norg/github; do
    if ! [ -d "$d" ]; then continue; fi
    pushd "$d" > /dev/null
    printf '\n'
    printf '=%.0s' $(seq ${#d})
    printf '\n'
    echo "${d}"
    printf '=%.0s' $(seq ${#d})
    printf '\n\n'
    git $*
    popd > /dev/null
  done
}

if test -x $HOME/.rbenv/bin/rbenv
then
  echo initializing rbenv
  export RUBY_BUILD_CURL_OPTS="-k"
  if ! echo $PATH | grep "$HOME/.rbenv/bin" > /dev/null
  then
    export PATH="$HOME/.rbenv/bin:$PATH"
  fi
  eval "$(~/.rbenv/bin/rbenv init - bash)"
fi

if test -r $HOME/.python3-env/bin/activate
then
  echo initializing python3-env
  source $HOME/.python3-env/bin/activate
fi

# This is needed to build the lua-language-server.
alias luamake=~/Src/lua-language-server/3rd/luamake/luamake


# EOF

