local M = {}

M.dap = {
  plugin = true,
  -- 'n' for normal mode
  n = {
    ["<leader>db"] = {
      "<cmd> DapToggleBreakpoint<CR>",
      "Toggle breakpoint at current line",
    },
    ["<leader>dr"] = {
      "<cmd> DapContinue <CR>",
      "Run (that is, start or continue) the debugger",
    },
  }
}

M.lsp = {
  n = {
    ["<leader>e"] = {
      ":lua vim.diagnostic.open_float(0, {scope='line'})<CR>",
      "Display error or warning."
    }
  }
}

return M
