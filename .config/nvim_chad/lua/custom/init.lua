local opt= vim.opt

opt.colorcolumn = '78'       -- Num of col after longest legal line.
opt.conceallevel = 1
opt.list = true              -- Show? some invisible characters
opt.shiftwidth = 3           -- Default level for indentation.
opt.tabstop = 8              -- Number of chars apparently taken up by tab.
opt.textwidth = 77           -- Columns before hard wrap.
--
-- undofile is turned off here because I like to be able to hit undo a bunch
-- of times to get back to how the file was when I opened it.  No matter how
-- many times I hit 'u', I do *not* want the file to revert to a state
-- *before* what it was when I opened it.
--
opt.undofile = false         -- Save? undo-history.
--
-- Do not mention 'h' and 'l', so that such horizontal
-- motion does not wrap past line-boundary.
--
opt.whichwrap = '<,>,[],b,s'
opt.wrap = false             -- Disable soft wrap.

vim.cmd 'highlight ColorColumn guibg=gray'

-- EOF

