
local plugins = {
  {
    -- Provide user-interface to debugging.
    "rcarriga/nvim-dap-ui",
    event = "VeryLazy",
    dependencies = "mfussenegger/nvim-dap",
    config = function()
      local dap = require("dap")
      local dapui = require("dapui")
      dapui.setup()
      dap.listeners.after.event_initialized["dapui_config"] = function()
        dapui.open()
      end
      dap.listeners.before.event_terminated["dapui_config"] = function()
        dapui.close()
      end
      dap.listeners.before.even_exited["dapui_config"] = function()
        dapui.close()
      end
    end
  },
  {
    -- Plug some holes between between mason and nvim-dap.
    "jay-babu/mason-nvim-dap.nvim",
    event = "VeryLazy",
    dependencies = {
      "williamboman/mason.nvim",
      "mfussenegger/nvim-dap",
    },
    opts = {
      -- Empty table indicates that we want to use defaults.
      handlers = {}
    },
  },
  {
    -- This plugin is for debugging via the debug-adapter protocol. It is
    -- used together with codelldb installed via mason.
    "mfussenegger/nvim-dap",
    config = function(_, _)
      require("core.utils").load_mappings("dap")
    end
  },
  {
      "mfussenegger/nvim-dap-python",
      ft = "python",
      dependencies = {
         "mfussenegger/nvim-dap",
         "rcarriga/nvim-dap-ui",
      },
      config = function (_, _)
         local path = "~/.local/share/nvim/mason/packages/debugpy/venv/bin/python"
         require("dap-python").setup(path)
         require("core.utils").load_mappings("dap-python")
      end,
  },
  {
    "neovim/nvim-lspconfig",
    -- It is recommended (as by https://nvchad.com/docs/config/format_lint)
    -- that one install null-ls to manage formatting and linting.
    dependencies = {
      "jose-elias-alvarez/null-ls.nvim",
      event = "VeryLazy",
      opts = function()
        return require "custom.configs.null-ls"
      end,
    },
    config = function()
      require "plugins.configs.lspconfig" -- from NvChad's base-configs
      require "custom.configs.lspconfig"  -- from my custom configs
    end,
  },
  {
    -- Mason is for installing each executable program, like a
    -- language-server, that neovim needs to interact with.
    "williamboman/mason.nvim",
    opts = {
      -- See `https://mason-registry.dev/registry/list`.
      ensure_installed = {
        "clangd",
          "pyright",
          "mypy",
          "ruff",
          "black",
          "debugpy",
      -------------------------------------------------------
        -- 
        -- clang-format fails to install via mason on ubuntu-18.04 because
        -- venv seems not to have 'wheel' installed.  So long as clang-format
        -- executable be found, functionality works. I have a python venv in
        -- which I was able to install it via 'pip install clang-format'.
        --
        "clang-format",
--        "cmake-language-server",
--        "cmakelang",
--        "latexindent",
--        "lua-language-server",
--        "stylua",
      }
    }
  }
}
return plugins

