# vim: set filetype=sh:
#
# This file, residing in a local copy of
# 'https://gitlab.com/tevaughan/config', is intended to be pointed to by a
# symlink, '~/.config/bash_aliases-wsl'.
#
# This file contains not just aliases but also other bashrc-type code
# appropriate only for my WSL-shell.
#
# The overall idea is that '~/.bashrc' should be a symlink to
# '/etc/skel/.bashrc', so that it is up to date with the operating system's
# idea of how the shell should be initialized.  At least on Debian-like
# systems, that idea presently involves sourcing '~/.bash_aliases' near the
# bottom of '/etc/skel/.bashrc'.  I use that as my hook to customize the
# configuration of the shell.
#
# My generic configuration, which is common to all of my shells, regardless
# of the particularities of the machine, is in '~/.bash_aliases', which is a
# symlink to `.bash_aliases` in a local copy of
# 'https://gitlab.com/tevaughan/config'.
#
# Each of '~/.config/bash_aliases*' is, in my scheme, intended to point to a
# machine-specific configuration-file that is sourced from '~/.bash_aliases'.

echo "${BASH_SOURCE[0]}: Vaughan's config for WSL"
umask o-w  # Disallow non-user, non-group permission to write.


ProgFiles="/mnt/c/Program Files"

Microsoft="${ProgFiles} (x86)/Microsoft"
Office="$Microsoft Office"

LibreOffice="${ProgFiles}/LibreOffice/program"


# Allow running LibreOffice Calc from bash command-line.
scalc() {
  "$LibreOffice"/scalc.exe $(wslpath -w $1)
}


# Allow running Windows-edge from bash command-line.
edge() {
  "$Microsoft"/Edge/Application/msedge.exe $(wslpath -w $1)
}


# Allow running Microsoft Excel from bash command-line.
excel() {
  "$Office"/root/Office16/EXCEL.EXE $(wslpath -w $1)
}


# Allow running Microsoft Word from bash command-line.
winword() {
  "$Office"/root/Office16/WINWORD.EXE $(wslpath -w $1)
}

matlab() {
  "$ProgFiles"/MATLAB/R2022a/bin/matlab.exe $(wslpath -w $1)
}


# EOF

